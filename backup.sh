#!/bin/bash

# Set backup directory
BACKUP_DIR="./backup"

# Set timestamp
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

# Backup database
backup_database() {
    echo "Backing up database..."
    docker-compose exec -T db mysqldump -u root -ppassword database > $BACKUP_DIR/db_backup_${TIMESTAMP}.sql
}

# Backup code
backup_code() {
    echo "Backing up code..."
    tar -czvf $BACKUP_DIR/code_backup_${TIMESTAMP}.tar.gz ./app
}

# Main function
main() {
    backup_database
    backup_code
    echo "Backup completed"
}

# Run main function
main
