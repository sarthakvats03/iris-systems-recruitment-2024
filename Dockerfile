# Use the official Ruby image from Docker Hub
FROM ruby:2.7.8

# Set the working directory in the container
WORKDIR /app

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
    build-essential \
    nodejs \
    npm \
    yarn \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install Bundler version 2.4.22
RUN gem install bundler -v 2.4.22

# Copy the Gemfile and Gemfile.lock into the container
COPY Gemfile Gemfile.lock ./

# Install gems
RUN bundle install

# Copy the rest of the application code into the container
COPY . .


# Command to run the Rails application
CMD ["sh", "-c", "./bin/rails server -b 0.0.0.0 -p $PORT"]
